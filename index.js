const db = require('./data/db')
const Employee = require('./data/model/Employee')
const Salary = require('./data/model/Salary')
const promise = require('bluebird')

/* Initial data */
const INITIAL_DATA = [
  { id: 106, age: 20, first: 'Rita', last: 'Tez' },
  { id: 107, age: 20, first: 'Sita', last: 'Synth' },
  { id: 108, age: 20, first: 'Jane', last: 'Eyre' }
]

/**
 * Adds salaries
 * @param {Array<Number>} ids The ids of the salaries
 * @param {Array<Number>} employeeIds The ids of the employees
 * @param {Array<Number>} amounts The amounts
 */
function addSalaries(ids, employeeIds, amounts) {
  return db.transaction(function (trx) {
    /* Queries */
    const queries = []

    /* Make queries */
    for (let index = 0; index < ids.length; index++) {
      const data = {
        id: ids[index],
        employee_id: employeeIds[index],
        amount: amounts[index]
      }
      queries.push(trx.insert(data).into('salary'))
    }

    /* Return the result of all queries */
    // no need to call `trx.commit()` or `trx.rollback()` as it's done automatically
    return Promise.all(queries)
  })
}

/**
 * Homework solution
 */
async function doHomework() {
  /* Add salaries */
  await addSalaries(
    [1, 2, 3],
    [106, 107, 108],
    [10, 9, 11]
  )

  /* Verify */
  const salaries = await Salary.fetchAll().map(s => s.models)
  console.log({ salaries })
}

async function main() {
  /* Clear all data */
  await Salary.collection().query().delete()
  await Employee.collection().query().delete()

  /* Insert initial data */
  await db.knex.insert(INITIAL_DATA, 'id').into('employees')

  /* Try doing the thing, clean up after all */
  try {
    await doHomework()
  } catch (e) {
    console.error(e)
  } finally {
    db.knex.destroy()
  }
}

main()
