// Setting up the database connection
const knex = require('knex')({
  client: 'pg',
  connection: {
    host     : '127.0.0.1',
    user     : 'postgres',
    password : 'root',
    database : 'company'
  }
})
const bookshelf = require('bookshelf')(knex)

module.exports = bookshelf
