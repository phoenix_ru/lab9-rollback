const bookshelf = require('../db')
const Employee = require('./Employee')

/*
 * CREATE TABLE salary(
 *  id INTEGER PRIMARY KEY,
 *  employee_id INT UNIQUE,
 *  amount INT,
 *  CONSTRAINT fk_employee
 *    FOREIGN KEY(employee_id)
 *      REFERENCES employees(id)
 *      ON DELETE SET NULL
 * );
 */
module.exports = bookshelf.model(
  'Salary',
  {
    tableName: 'salary',
    employee: function() {
      return this.hasOne(Employee)
    }
  }
)
