const bookshelf = require('../db')

/*
 * CREATE TABLE Employees(id INTEGER PRIMARY KEY, age INT, first TEXT, last TEXT);
 */
module.exports = bookshelf.model(
  'Employee',
  { tableName: 'employees' }
)
